<?php

namespace Models;

use DateTime;
use Models\User\IUser;

class User implements IUser {

    public int $id;
    public string $username;
    public string $email;
    public DateTime $registeredAt;
    public DateTime $birthday;

}
