<?php

use DateTime;

namespace Models\User;

interface IUser 
{
    public int $id;
    public string $username;
    public string $email;
    public DateTime $registeredAt;
    public DateTime $birthday;
}