<?php

namespace Engine\Request;

class Request implements Irequest {

    function __construct()
    {
        $this->bootstrapSelf();    
    }

    private function bootstrapSelf()
    {
        foreach ($_SERVER as $key => $value) {
            $this->{$this->toCamelCase($key)} = $value;    
        }
    }

    private function toCamelCase($str)
    {
        $result = strtolower($str);
        preg_match_all('/_[a-z]/', $result, $matches);
        
        foreach ($matches[0] as $match) {
            $capital = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $capital, $result);
        }
        return $result;
    }

    public function getBody()
    {
        if ($this->requestMethod === 'GET')
        {
            $body = [];
            foreach ($_GET as $key => $value) {
                $body[$key] = filter_input(INPUT_GET, $value, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $body;
        }
        if ($this->requestMethod === 'POST')
        {
            $body = [];
            foreach ($_POST as $key => $value) {
                $body[$key] = filter_input(INPUT_POST, $body, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $body;
        }
    }
}
