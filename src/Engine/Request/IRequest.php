<?php

namespace Engine\Request;

interface IRequest {
    public function getBody() : array;
}
