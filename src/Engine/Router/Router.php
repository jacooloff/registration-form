<?php
use Controllers;
use Engine\Request\IRequest;
class Router 
{
    private $request;
    private $supportedHttpMethods = array(
        "GET",
        "POST"
    );
    
    function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    public function __call($name, $args) 
    {
        list($route, $controllerAndMethod) = $args;
        
        if(!in_array(strtoupper($name), $this->supportedHttpMethods))
        {
            return $this->invalidMethodHandler();
        }

        $this->{strtolower($name)}[$this->formatRoute($route)] = $controllerAndMethod;
    }

    private function formatRoute($route)
    {
        $result = rtrim($route, "/");
        $result = str_replace("?{$_SERVER['QUERY_STRING']}", '', $result);
        if ($result === '') {
            return '/';
        }
        
        return $result;
    }

    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }

    private function defaultRequestHandler()
    {
        header("{$this->request->serverProtocol} 404 Not Found");
    }

    function resolve() 
    {
        $methodDictionary = $this->{strtolower($this->request->requestMethod)};
        $formatedRoute = $this->formatRoute($this->request->requestUri);
        $controllerAndMethod = $methodDictionary[$formatedRoute];
     
        if(is_null($controllerAndMethod))
        {
            $this->defaultRequestHandler();
            return;
        }
        $controllerAndMethod = explode('@', $controllerAndMethod);
        $controller = $controllerAndMethod[0];
        $method = $controllerAndMethod[1];
        $methodImplementation = ReflectionClass($controller, $method);
        $methodImplementation($this->request);
        // echo call_user_func_array($method, array($this->request));
    }

    function __destruct()
    {
        $this->resolve();
    }
}