<?php

use Models\IUser;

namespace Controllers;

class Controller 
{
    function register(IUser $user, Register\Handler $handler)
    {
        $handler->handle($user);
        return 'Please confirm';
    }

    function confirm(string $code, Confirm\Handler $handler)
    {
        $handler->handle($code);
        return 'You have been registered';        
    }

    function showProfile(ReadModel\ReadProfile $handler)
    {
        $handler->handle();
        return 'Profile';
    }
}