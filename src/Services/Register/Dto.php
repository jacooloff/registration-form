<?php

namespace Services\Register;

use DateTime;

class Dto {

    public string $username;
    public string $email;
    public DateTime $birthday;

    function __construct(IRequest $request)
    {
        $this->username = $request->getBody()['username'];
        $this->email = $request->getBody()['email'];
        $this->birthday = $request->getBody()['birthday'];
    }
}
